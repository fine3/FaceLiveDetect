package com.oliveapp.face.livenessdetectionviewsdk.verification_controller;

import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.Size;
import android.os.Handler;

import com.oliveapp.camerasdk.CameraManager.CameraPreviewDataCallback;
import com.oliveapp.camerasdk.CameraManager.CameraProxy;
import com.oliveapp.face.livenessdetectionviewsdk.event_interface.ViewUpdateEventHandlerIf;
import com.oliveapp.face.livenessdetectorsdk.datatype.AccessInfo;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.LivenessDetector;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.LivenessStatusListenerIf;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.FrameData;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.ImageProcessParameter;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.LivenessDetectionFrames;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.LivenessDetectorConfig;
import com.oliveapp.face.livenessdetectorsdk.prestartvalidator.PrestartEventListenerIf;
import com.oliveapp.face.livenessdetectorsdk.prestartvalidator.PrestartValidator;
import com.oliveapp.face.livenessdetectorsdk.prestartvalidator.datatype.PrestartDetectionFrame;
import com.oliveapp.face.livenessdetectorsdk.utilities.utils.LogUtil;

public class VerificationController implements CameraPreviewDataCallback, LivenessStatusListenerIf, PrestartEventListenerIf {
    private static final String a = VerificationController.class.getSimpleName();
    private Activity b;
    private Handler c;
    private AccessInfo d;
    private ViewUpdateEventHandlerIf e;
    private PrestartValidator f;
    private LivenessDetector g;
    private int h = -1;
    public static final int STEP_READY = 0;
    public static final int STEP_PRESTART = 1;
    public static final int STEP_LIVENESS = 2;
    public static final int STEP_FINISH = 3;
    private int i = 0;
    private boolean j = false;
    private int k = 0;

    public VerificationController(AccessInfo accessInfo, ViewUpdateEventHandlerIf viewUpdateEventHandlerIf, PrestartValidator prestartValidator, LivenessDetector livenessDetector, Activity activity, Handler handler) {
        this.d = accessInfo;
        this.e = viewUpdateEventHandlerIf;
        this.f = prestartValidator;
        this.g = livenessDetector;
        this.b = activity;
        this.c = handler;
    }

    public VerificationController(AccessInfo accessInfo, ViewUpdateEventHandlerIf viewUpdateEventHandlerIf, final ImageProcessParameter imageProcessParameter, final LivenessDetectorConfig livenessDetectorConfig, Activity activity, Handler handler, final int mode) {
        this.b = activity;
        this.c = handler;
        this.e = viewUpdateEventHandlerIf;
        this.d = accessInfo;
        Thread var8 = new Thread(new Runnable() {
            public void run() {
                try {
                    VerificationController.this.f = new PrestartValidator();
                    LivenessDetectorConfig var1 = new LivenessDetectorConfig();
                    var1.usePredefinedConfig(4);
                    var1.timeoutMs = 2147483647;
                    VerificationController.this.f.init(VerificationController.this.b, VerificationController.this.c, VerificationController.this, imageProcessParameter, var1);
                    VerificationController.this.g = new LivenessDetector();
                    VerificationController.this.g.init(VerificationController.this.b, VerificationController.this.c, VerificationController.this, imageProcessParameter, livenessDetectorConfig, mode);
                    VerificationController.this.i = 0;
                } catch (final Exception var2) {
                    LogUtil.e(VerificationController.a, "无法初始化LivenessDetector...", var2);
                    VerificationController.this.c.post(new Runnable() {
                        public void run() {
                            try {
                                VerificationController.this.e.onInitializeFail(var2);
                            } catch (Exception var2x) {
                                LogUtil.e(VerificationController.a, "onInitializeFail函数出错，请检查您的事件处理代码", var2x);
                            }

                        }
                    });
                }

                VerificationController.this.c.post(new Runnable() {
                    public void run() {
                        try {
                            VerificationController.this.e.onInitializeSucc();
                        } catch (Exception var2) {
                            LogUtil.e(VerificationController.a, "onInitializeSucc函数出错，请检查您的事件处理代码", var2);
                        }

                    }
                });
            }
        });
        var8.setName("InitControllerThread");
        var8.start();
    }

    public int getCurrentStep() {
        return this.i;
    }

    public void nextVerificationStep() {
        switch(this.i) {
            case 0:
                this.i = 1;
                break;
            case 1:
                this.i = 2;

                try {
                    this.f.uninit();
                    this.f = null;
                } catch (Exception var2) {
                    LogUtil.e(a, "无法销毁预检测对象...", var2);
                }
                break;
            case 2:
                this.i = 3;
            case 3:
        }

    }

    public void enterLivenessDetection() {
        if (this.i == 1 && this.j) {
            this.nextVerificationStep();
        }

    }

    public void nextAction() {
        if (this.g != null) {
            this.g.nextAction();
        }

    }

    public void uninit() {
        try {
            this.b();
            this.g = null;
            this.f = null;
            this.b = null;
            this.c = null;
            this.e = null;
        } catch (Exception var2) {
            LogUtil.e(a, "无法销毁VerificationManager...", var2);
        }

    }

    private void b() {
        try {
            if (this.f != null) {
                this.f.uninit();
            }
        } catch (Exception var3) {
            LogUtil.e(a, "无法销毁预检测对象...", var3);
        }

        try {
            if (this.g != null) {
                this.g.uninit();
            }
        } catch (Exception var2) {
            LogUtil.e(a, "无法销毁活体检测对象...", var2);
        }

        this.f = null;
        this.g = null;
    }

    public void onPreviewFrame(byte[] data, CameraProxy camera, int faces) {
        if (FrameData.sImageConfigForVerify != null) {
            boolean var5;
            if (this.h == -1) {
                CameraInfo var4 = new CameraInfo();
                var5 = false;
                int var6 = Camera.getNumberOfCameras();
                byte var7 = 1;

                for(int var16 = 0; var16 < var6; ++var16) {
                    Camera.getCameraInfo(var16, var4);
                    if (var4.facing == var7) {
                        break;
                    }
                }

                int var8 = this.b.getWindowManager().getDefaultDisplay().getRotation();
                short var9 = 0;
                switch(var8) {
                    case 0:
                        var9 = 0;
                        break;
                    case 1:
                        var9 = 90;
                        break;
                    case 2:
                        var9 = 180;
                        break;
                    case 3:
                        var9 = 270;
                }

                int var10;
                if (var4.facing != 1 && var6 != 1) {
                    var10 = (var4.orientation - var9 + 360) % 360;
                } else {
                    var10 = (var4.orientation + var9) % 360;
                    var10 = (360 - var10) % 360;
                }

                try {
                    this.h = var10;
                    FrameData.sImageConfigForVerify.setPreRotationDegree(this.h);
                } catch (Exception var14) {
                }

                LogUtil.i(a, "Camera Rotation: " + this.h + " & info.facing: " + var4.facing);
            }

            ++this.k;
            if (this.k < 10) {
                LogUtil.i(a, "onPreviewFrame, drop frame id: " + this.k);
            } else {
                LogUtil.i(a, "[BEGIN] onPreviewFrame, frame id: " + this.k);
                Size var15 = camera.getParameters().getPreviewSize();
                var5 = false;
                switch(this.i) {
                    case 0:
                    default:
                        break;
                    case 1:
                        try {
                            LogUtil.i(a, "mPrestartValidator.doDetection...");
                            var5 = this.f.doDetection(data, var15.width, var15.height);
                        } catch (Exception var13) {
                            LogUtil.e(a, "[预检模块] 无法处理当前帧...", var13);
                        }
                        break;
                    case 2:
                        try {
                            LogUtil.i(a, "mLivenessDetector.doDetection...");
                            var5 = this.g.doDetection(data, var15.width, var15.height);
                        } catch (Exception var12) {
                            LogUtil.e(a, "[活体检测] 无法处理当前帧...", var12);
                        }
                }

                LogUtil.i(a, "[END] onPreviewFrame, 当前帧处理是否处理成功: " + var5);
            }
        }
    }

    public void onPrestartFrameDetected(PrestartDetectionFrame frame, int remainingTimeMillisecond) {
        this.e.onPrestartFrameDetected(frame, remainingTimeMillisecond);
    }

    public void onPrestartSuccess(LivenessDetectionFrames livenessDetectionFrames) {
        LogUtil.i(a, "[BEGIN] onPrestartSuccess");
        this.j = true;
        this.e.onPrestartSuccess(livenessDetectionFrames);
        LogUtil.i(a, "[END] onPrestartSuccess");
    }

    public void onPrestartFail(int result) {
        LogUtil.i(a, "[BEGIN] onPrestartFail");
        LogUtil.i(a, "[END] onPrestartFail");
    }

    public void onLivenessSuccess(LivenessDetectionFrames livenessDetectionFrames) {
        this.e.onLivenessSuccess(livenessDetectionFrames);
    }

    public void onLivenessFail(int result, LivenessDetectionFrames livenessDetectionFrames) {
        this.e.onLivenessFail(result, livenessDetectionFrames);
    }

    public void onActionChanged(int lastActionType, int lastActionResult, int newActionType, int currentActionIndex) {
        this.e.onActionChanged(lastActionType, lastActionResult, newActionType, currentActionIndex);
    }

    public void onFrameDetected(int currentActionType, int actionState, int sessionState, int remainingTimeoutSecond) {
        this.e.onFrameDetected(currentActionType, actionState, sessionState, remainingTimeoutSecond);
    }
}
