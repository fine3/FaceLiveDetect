package com.oliveapp.face.livenessdetectionviewsdk.utils;

import android.media.MediaPlayer;

import com.oliveapp.face.livenessdetectorsdk.utilities.utils.LogUtil;

public class AudioModule {
    private static final String a = AudioModule.class.getSimpleName();
    private MediaPlayer b = new MediaPlayer();

    public AudioModule() {
    }

    public boolean isPlaying() {
        return this.b != null && this.b.isPlaying();
    }

    public void release() {
        try {
            this.b.stop();
            this.b.release();
            this.b = null;
        } catch (Exception var2) {
            LogUtil.e(a, "Fail to release", var2);
        }

    }
}
