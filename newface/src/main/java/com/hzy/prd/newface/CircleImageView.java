package com.hzy.prd.newface;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.util.AttributeSet;

import java.util.List;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.appcompat.widget.AppCompatImageView;

public class CircleImageView extends AppCompatImageView {
    private static final ScaleType b;
    private static final Bitmap.Config c;
    private final RectF d;
    private final RectF e;
    private final Matrix f;
    private final Paint g;
    private final Paint h;
    private final Paint i;
    private int j;
    private int k;
    private int l;
    private Bitmap m;
    private BitmapShader n;
    private int o;
    private int p;
    private float q;
    private float r;
    private ColorFilter s;
    private boolean t;
    private boolean u;
    private boolean v;
    private Thread x;
    private List<String> y;
    private int z;
    private int A;
    private final String packageName;

    public CircleImageView(Context context) {
        super(context);
        packageName = context.getPackageName();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Matrix();
        this.g = new Paint();
        this.h = new Paint();
        this.i = new Paint();
        this.j = -16777216;
        this.k = 0;
        this.l = 0;
        this.z = 0;
        this.A = 1000;
        this.b();
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        packageName = context.getPackageName();
        this.d = new RectF();
        this.e = new RectF();
        this.f = new Matrix();
        this.g = new Paint();
        this.h = new Paint();
        this.i = new Paint();
        this.j = -16777216;
        this.k = 0;
        this.l = 0;
        this.z = 0;
        this.A = 1000;
        TypedArray var5 = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView);
        this.o = var5.getDimensionPixelSize(R.styleable.CircleImageView_oliveapp_civ_border_width, 0);
        this.j = var5.getColor(R.styleable.CircleImageView_oliveapp_civ_border_color, -16777216);
        this.v = var5.getBoolean(R.styleable.CircleImageView_oliveapp_civ_border_overlay, false);
        this.l = var5.getColor(R.styleable.CircleImageView_oliveapp_civ_fill_color, 0);
        var5.recycle();
        this.b();
    }

    private void b() {
        super.setScaleType(b);
        this.t = true;
        if (this.u) {
            this.c();
            this.u = false;
        }

    }

    public ScaleType getScaleType() {
        return b;
    }

    public void setScaleType(ScaleType scaleType) {
        if (scaleType != b) {
            throw new IllegalArgumentException(String.format("ScaleType %s not supported.", scaleType));
        }
    }

    public void setAdjustViewBounds(boolean adjustViewBounds) {
        if (adjustViewBounds) {
            throw new IllegalArgumentException("adjustViewBounds not supported.");
        }
    }

    protected void onDraw(Canvas canvas) {
        if (this.m != null) {
            if (this.l != 0) {
                canvas.drawCircle((float) this.getWidth() / 2.0F,
                        (float) this.getHeight() / 2.0F, this.q, this.i);
            }
            canvas.drawCircle((float) this.getWidth() / 2.0F,
                    (float) this.getHeight() / 2.0F, this.q, this.g);
            if (this.k != 0) {
                canvas.drawCircle((float) this.getWidth() / 2.0F,
                        (float) this.getHeight() / 2.0F, this.r, this.h);
            }

        }
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.c();
    }

    public int getBorderColor() {
        return this.j;
    }

    public void setBorderColor(@ColorInt int borderColor) {
        if (borderColor != this.j) {
            this.j = borderColor;
            this.h.setColor(this.j);
            this.invalidate();
        }
    }

    public void setBorderColorResource(@ColorRes int borderColorRes) {
        this.setBorderColor(this.getContext().getResources().getColor(borderColorRes));
    }

    public int getFillColor() {
        return this.l;
    }

    public void setFillColor(@ColorInt int fillColor) {
        if (fillColor != this.l) {
            this.l = fillColor;
            this.i.setColor(fillColor);
            this.invalidate();
        }
    }

    public void setFillColorResource(@ColorRes int fillColorRes) {
        this.setFillColor(this.getContext().getResources().getColor(fillColorRes));
    }

    public int getBorderWidth() {
        return this.k;
    }

    public void setBorderWidth(int borderWidth) {
        if (borderWidth != this.k) {
            this.k = borderWidth;
            this.c();
        }
    }

    public boolean isBorderOverlay() {
        return this.v;
    }

    public void setBorderOverlay(boolean borderOverlay) {
        if (borderOverlay != this.v) {
            this.v = borderOverlay;
            this.c();
        }
    }

    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        this.m = bm;
        this.c();
    }

    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(drawable);
        this.m = this.a(drawable);
        this.c();
    }

    public void setImageResource(@DrawableRes int resId) {
        super.setImageResource(resId);
        this.m = this.a(this.getDrawable());
        this.c();
    }

    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
        this.m = uri != null ? this.a(this.getDrawable()) : null;
        this.c();
    }

    public void setColorFilter(ColorFilter cf) {
        if (cf != this.s) {
            this.s = cf;
            this.g.setColorFilter(this.s);
            this.invalidate();
        }
    }

    private Bitmap a(Drawable var1) {
        if (var1 == null) {
            return null;
        } else if (var1 instanceof BitmapDrawable) {
            return ((BitmapDrawable) var1).getBitmap();
        } else {
            try {
                Bitmap var2;
                if (var1 instanceof ColorDrawable) {
                    var2 = Bitmap.createBitmap(2, 2, c);
                } else {
                    var2 = Bitmap.createBitmap(var1.getIntrinsicWidth(), var1.getIntrinsicHeight(), c);
                }

                Canvas var3 = new Canvas(var2);
                var1.setBounds(0, 0, var3.getWidth(), var3.getHeight());
                var1.draw(var3);
                return var2;
            } catch (Exception var4) {
                var4.printStackTrace();
                return null;
            }
        }
    }

    private void c() {
        if (!this.t) {
            this.u = true;
        } else if (this.getWidth() != 0 || this.getHeight() != 0) {
            if (this.m == null) {
                this.invalidate();
            } else {
                this.n = new BitmapShader(this.m, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP);
                this.g.setAntiAlias(true);
                this.g.setShader(this.n);
                this.h.setStyle(Paint.Style.STROKE);
                this.h.setAntiAlias(true);
                this.h.setColor(this.j);
                this.h.setStrokeWidth((float) this.k);
                this.i.setStyle(Paint.Style.FILL);
                this.i.setAntiAlias(true);
                this.i.setColor(this.l);
                this.p = this.m.getHeight();
                this.o = this.m.getWidth();
                this.e.set(0.0F, 0.0F, (float) this.getWidth(), (float) this.getHeight());
                this.r = Math.min((this.e.height() - (float) this.k) / 2.0F, (this.e.width() - (float) this.k) / 2.0F);
                this.d.set(this.e);
                if (!this.v) {
                    this.d.inset((float) this.k, (float) this.k);
                }

                this.q = Math.min(this.d.height() / 2.0F, this.d.width() / 2.0F);
                this.d();
                this.invalidate();
            }
        }
    }

    private void d() {
        float var2 = 0.0F;
        float var3 = 0.0F;
        this.f.set(null);
        float var1;
        if ((float) this.o * this.d.height() > this.d.width() * (float) this.p) {
            var1 = this.d.height() / (float) this.p;
            var2 = (this.d.width() - (float) this.o * var1) * 0.5F;
        } else {
            var1 = this.d.width() / (float) this.o;
            var3 = (this.d.height() - (float) this.p * var1) * 0.5F;
        }

        this.f.setScale(var1, var1);
        this.f.postTranslate((float) ((int) (var2 + 0.5F)) + this.d.left, (float) ((int) (var3 + 0.5F)) + this.d.top);
        this.n.setLocalMatrix(this.f);
    }

    public void start() {
        this.stop();
        this.x = new Thread(new Runnable() {
            public void run() {
                while (true) {
                    if (!Thread.interrupted()) {
                        try {
                            Thread.sleep(CircleImageView.this.A);
                            if (CircleImageView.this.y != null && CircleImageView.this.y.size() != 0) {
                                final int var1 = CircleImageView.this.getResources()
                                        .getIdentifier(CircleImageView.this.y.get(CircleImageView.this.z), "drawable", CircleImageView.this.packageName);
                                CircleImageView.this.post(new Runnable() {
                                    public void run() {
                                        try {
                                            CircleImageView.this.setImageResource(var1);
                                        } catch (Exception var2) {
                                        }

                                    }
                                });
                                CircleImageView.this.z = (CircleImageView.this.z + 1) % CircleImageView.this.y.size();
                            }
                            continue;
                        } catch (InterruptedException var2) {
                        } catch (Exception var3) {
                            continue;
                        }
                    }

                    return;
                }
            }
        });
        this.x.setName("GifTransitionThread");
        this.x.start();
    }

    public void stop() {
        if (this.x != null) {
            this.x.interrupt();
            try {
                this.x.join();
            } catch (InterruptedException var2) {
            }
        }

    }

    public void updateAnimation(List<String> imageList, int intervalMillisecond) {
        this.y = imageList;
        this.A = intervalMillisecond;
        this.z = 0;
        this.post(() -> {
            try {
                int var1 = CircleImageView.this.getResources().getIdentifier(CircleImageView.this.y.get(0), "drawable", CircleImageView.this.packageName);
                CircleImageView.this.setImageResource(var1);
            } catch (Exception var2) {
            }

        });
    }

    static {
        b = ScaleType.CENTER_CROP;
        c = Bitmap.Config.ARGB_8888;
    }
}
