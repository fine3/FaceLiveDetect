package com.hzy.prd.newface;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import com.oliveapp.face.livenessdetectorsdk.utilities.utils.LogUtil;

public class FixedAspectLayout extends FrameLayout {
    private float a = 1.0F;
    private int b = 1;

    public FixedAspectLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.a(context, attrs);
    }

    private void a(Context var1, AttributeSet var2) {
        TypedArray var6 = var1.obtainStyledAttributes(var2, R.styleable.FixedAspectLayout);
        this.a = var6.getFloat(R.styleable.FixedAspectLayout_oliveapp_aspectRatio, 1.0F);
        this.b = var6.getInt(R.styleable.FixedAspectLayout_oliveapp_fixMode, 1);
        var6.recycle();
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int var3 = MeasureSpec.getSize(widthMeasureSpec);
        int var4 = MeasureSpec.getSize(heightMeasureSpec);
        if (var3 == 0) {
            var4 = 0;
        } else {
            switch (this.b) {
                case 1:
                    var3 = (int) ((float) var4 / this.a);
                    break;
                case 2:
                    var4 = (int) ((float) var3 * this.a);
                    break;
                default:
                    var3 = (int) ((float) var4 / this.a);
                    LogUtil.w("FixedAspectLayout", "unknown fix mode: " + this.b);
            }
        }

        super.onMeasure(MeasureSpec.makeMeasureSpec(var3, MeasureSpec.getMode(widthMeasureSpec)),
                MeasureSpec.makeMeasureSpec(var4, MeasureSpec.getMode(heightMeasureSpec)));
    }
}
