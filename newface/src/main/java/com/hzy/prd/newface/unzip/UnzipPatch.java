package com.hzy.prd.newface.unzip;

public interface UnzipPatch {
    void unzip(String src, String dst) throws Exception;
}
