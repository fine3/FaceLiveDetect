package com.hzy.prd.newface;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class CountDownCircle extends RelativeLayout {

    private long maxTimeout = 12_000L;
    private ProgressBar progressBar;
    private TextView textView;
    private ObjectAnimator objectAnimator;

    public CountDownCircle(Context context) {
        super(context);
        this.a();
    }

    public CountDownCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.a();
    }

    public CountDownCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.a();
    }

    private void a() {
        inflate(this.getContext(), R.layout.oliveapp_circular_count_down_progress_bar, this);
        this.progressBar = this.findViewById(R.id.oliveapp_timeoutProgressbar);
        this.textView = this.findViewById(R.id.oliveapp_countdownTextView);
    }

    public void start(long countDownMillisecond) {
        if (this.objectAnimator != null) {
            this.objectAnimator.cancel();
        }
        this.objectAnimator = ObjectAnimator.ofInt(this.progressBar, "progress",
                1000, 0);
        this.objectAnimator.setDuration(countDownMillisecond);
        this.objectAnimator.setInterpolator(new LinearInterpolator());
        this.maxTimeout = countDownMillisecond;
        this.objectAnimator.start();

        try {
            this.textView.postDelayed(new Runnable() {
                public void run() {
                    if (CountDownCircle.this.progressBar != null && CountDownCircle.this.textView != null) {
                        try {
                            CountDownCircle.this.b();
                            CountDownCircle.this.textView.postDelayed(this, 400L);
                        } catch (NullPointerException var2) {
                        }

                    }
                }
            }, 200L);
        } catch (NullPointerException var4) {
        }

    }

    public void stop() {
        if (this.objectAnimator != null) {
            this.objectAnimator.cancel();
        }

    }

    public void destory() {
        this.stop();
        this.textView = null;
        this.progressBar = null;
        this.objectAnimator = null;
        this.objectAnimator = null;
    }

    public void restart() {
        this.stop();
        this.start(this.maxTimeout);
    }

    public int getProgress() {
        return this.progressBar.getProgress();
    }

    public void setProgress(int progress) {
        this.progressBar.setProgress(progress);
    }

    private void b() {
        long var1 = (long) Math.ceil((float) this.maxTimeout / 1000.0F *
                (float) this.progressBar.getProgress() / (float) this.progressBar.getMax());
        if (var1 <= 0L) {
        } else {
            this.textView.setTextKeepState(var1 + "S");
        }
    }

    public void setRemainTimeSecond(int remainTimeMilliSecond, int totalTimeMilliSecond) {
        this.maxTimeout = totalTimeMilliSecond;
        this.progressBar.setProgress(1000 * remainTimeMilliSecond / totalTimeMilliSecond);
        this.b();
    }
}
