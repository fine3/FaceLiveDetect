package com.hzy.prd.newface;

import android.content.Context;

import com.oliveapp.face.livenessdetectorsdk.livenessdetector.configs.ApplicationParameters;

public class CaptureUtils {

    public static void init(Context context) {
        try {
            String cachePath = context.getCacheDir().getAbsolutePath();
            ApplicationParameters.setStoragePath(cachePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
