package com.hzy.prd.newface;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.view.View;
import android.widget.TextView;

import com.oliveapp.camerasdk.PhotoModule;
import com.oliveapp.camerasdk.utils.CameraUtil;
import com.oliveapp.face.livenessdetectionviewsdk.event_interface.ViewUpdateEventHandlerIf;
import com.oliveapp.face.livenessdetectionviewsdk.utils.AudioModule;
import com.oliveapp.face.livenessdetectionviewsdk.verification_controller.VerificationController;
import com.oliveapp.face.livenessdetectorsdk.datatype.AccessInfo;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.FacialActionType;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.ImageProcessParameter;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.LivenessDetectionFrames;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.LivenessDetectorConfig;
import com.oliveapp.face.livenessdetectorsdk.prestartvalidator.datatype.PrestartDetectionFrame;
import com.oliveapp.face.livenessdetectorsdk.utilities.utils.PackageNameManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class BaseDetectionActivity extends Activity
        implements ViewUpdateEventHandlerIf {

    private static final float TARGET_PREVIEW_RATIO = 1.3333334F;
    private static final int MAX_PREVIEW_WIDTH = 961;

    protected PhotoModule mPhotoModule;
    protected AudioModule mAudioModule;
    private int mActionChangeMode;
    private CircleImageView mOliveappStepHintImage;
    private TextView mOliveappStepHintText;
    private CountDownCircle mOliveappCountDownProgressbar;
    private VerificationController mVerificationController;
    private Handler mCameraHandler;
    protected HandlerThread mCameraHandlerThread;
    protected ImageProcessParameter mImageProcessParameter;
    protected LivenessDetectorConfig mLivenessDetectorConfig;

    protected void onCreate(Bundle savedInstanceState) {
        CaptureUtils.init(this);
        if (!PackageNameManager.isPackageNameSet()) {
            PackageNameManager.setPackageName(getPackageName());
        }
        com.oliveapp.camerasdk.utils.PackageNameManager
                .setPackageName(PackageNameManager.getPackageName());
        super.onCreate(savedInstanceState);
        mActionChangeMode = 2;
        initViews();
        initCamera();
        initControllers();
    }

    protected void onResume() {
        super.onResume();
        if (mPhotoModule != null) {
            mPhotoModule.onResume();
        } else {
            onLivenessFail(5, null);
        }
        try {
            mPhotoModule.setPreviewDataCallback(mVerificationController,
                    mCameraHandler);
        } catch (NullPointerException ignored) {
        }
        if (mAudioModule != null) {
            playAudio("oliveapp_step_hint_getready");
        }
    }

    protected void onPause() {
        super.onPause();
        if (mPhotoModule != null) {
            mPhotoModule.onPause();
        }
    }

    protected void onStop() {
        super.onStop();
        stopCamera();
    }

    private void stopCamera() {
        if (mPhotoModule != null) {
            mPhotoModule.onStop();
        }
        CameraUtil.sContext = null;
        mPhotoModule = null;
        if (mAudioModule != null) {
            mAudioModule.release();
            mAudioModule = null;
        }
        if (mOliveappStepHintImage != null) {
            mOliveappStepHintImage.stop();
        }
        mOliveappStepHintImage = null;
        if (mCameraHandlerThread != null) {
            try {
                mCameraHandlerThread.quit();
                mCameraHandlerThread.join();
            } catch (InterruptedException ignored) {
            }
        }
        mCameraHandlerThread = null;
        if (mVerificationController != null) {
            mVerificationController.uninit();
        }
        mVerificationController = null;
        if (mOliveappCountDownProgressbar != null) {
            mOliveappCountDownProgressbar.destory();
        }
        mOliveappCountDownProgressbar = null;
    }

    private void initCamera() {
        int cameraCount = Camera.getNumberOfCameras();
        CameraInfo cameraInfo = new CameraInfo();
        int expectCameraFacing = CameraInfo.CAMERA_FACING_FRONT;
        for (int camIdx = 0; camIdx < cameraCount; ++camIdx) {
            Camera.getCameraInfo(camIdx, cameraInfo);
            if (cameraInfo.facing == expectCameraFacing) {
                getIntent().putExtra(CameraUtil.EXTRAS_CAMERA_FACING, camIdx); // 设置需要打开的摄像头ID
                getIntent().putExtra(CameraUtil.MAX_PREVIEW_WIDTH, MAX_PREVIEW_WIDTH); // 设置最大Preview宽度
                getIntent().putExtra(CameraUtil.TARGET_PREVIEW_RATIO, TARGET_PREVIEW_RATIO); // 设置Preview长宽比
                break;
            }
        }
        mPhotoModule = new PhotoModule();
        mPhotoModule.init(this, findViewById(R.id.oliveapp_cameraPreviewView));
        mPhotoModule.setPlaneMode(false, false);
        mPhotoModule.onStart();
        mCameraHandlerThread = new HandlerThread("CameraHandlerThread");
        mCameraHandlerThread.start();
        mCameraHandler = new Handler(mCameraHandlerThread.getLooper());
    }

    private void initViews() {
        setContentView(R.layout.oliveapp_activity_liveness_detection_main);
        mOliveappStepHintImage = findViewById(R.id.oliveapp_step_hint_image);
        mOliveappStepHintImage.start();
        mOliveappStepHintText = findViewById(R.id.oliveapp_step_hint_text);
        mOliveappCountDownProgressbar = findViewById(R.id.oliveapp_step_countdown_progressbar);
        mOliveappCountDownProgressbar.setVisibility(View.INVISIBLE);
        mAudioModule = new AudioModule();
    }

    private void setDetectionParameter() throws Exception {
        /**
         * 注意: 默认参数适合手机，一般情况下不需要修改这些参数。如需修改请联系百融工程师
         *
         * 设置从preview图片中截取人脸框的位置，调用doDetection前必须调用本函数。
         * @param preRotationDegree　逆时针旋转角度，只允许0 90 180 270，大部分手机应当是90
         *                         以下说明中的帧都是指旋转后的帧 旋转之后的帧建议宽高比例3:4，否则算法结果无法保证
         * @param cropWidthPercent　截取的人脸框宽度占帧宽度的比例
         * @param verticalOffsetPercent　截取的人脸框上边缘到帧上边缘的距离占帧高度的比例，
         *                             算法真正截取的人脸的尺寸=cropWidthPercent*4/3，再依据这个参数获取需要截取的位置
         * @param shouldFlip 是否左右翻转。一般前置摄像头为false
         */
        mImageProcessParameter = new ImageProcessParameter(false, 1.0f,
                0.0f, 90);
        /**
         * 动作生成规则和通过规则配置
         * 注意: 默认参数平衡了通过率和防Hack能力，如需修改配置建议咨询百融工程师
         * SDK提供了三种配置方式
         */
        // 使用预设配置: 满足绝大多数常见场景
        mLivenessDetectorConfig = new LivenessDetectorConfig();
        // 默认配置为: 3个动作，不允许动作失败，10s超时
        // 使用预设配置: 满足绝大多数常见场景
        // 两个动作，头向左转，眨眼睛
        mLivenessDetectorConfig.usePredefinedConfig(0);
        mLivenessDetectorConfig.timeoutMs = 12_000;
        mLivenessDetectorConfig.fixedActions = true;
        mLivenessDetectorConfig.fixedActionList = Arrays.asList(
                FacialActionType.EYE_CLOSE,
                FacialActionType.HEAD_LEFT,
                FacialActionType.CAPTURE);
        if (mLivenessDetectorConfig != null)
            mLivenessDetectorConfig.validate();
    }

    public void setDetectionActions(int ... args){
        List<Integer> fixedActionList = new ArrayList();
        if (args != null) {
            for (int action:args) {
                fixedActionList.add(action);
            }
            mLivenessDetectorConfig.fixedActionList = fixedActionList;
        }
    }

    public void setUsePredefinedConfig(int solutionId) {
        if (mLivenessDetectorConfig != null) {
            mLivenessDetectorConfig.usePredefinedConfig(solutionId);
        }
    }

    private void initControllers() {
        try {
            setDetectionParameter();
            mVerificationController = new VerificationController(AccessInfo.getInstance(),
                    this, mImageProcessParameter,
                    mLivenessDetectorConfig, this,
                    new Handler(Looper.getMainLooper()), mActionChangeMode);
        } catch (Exception var2) {
        }
    }

    public void startVerification() {
        try {
            if (mVerificationController.getCurrentStep() == 0) {
                mVerificationController.nextVerificationStep();
            }
        } catch (Exception var2) {
        }

    }

    public void onInitializeSucc() {
    }

    public void onInitializeFail(Throwable e) {
    }

    public void onActionChanged(int lastActionType, final int lastActionResult,
                                final int newActionType, int currentActionIndex) {
        try {
            if (mActionChangeMode == 2) {
                mOliveappCountDownProgressbar.setVisibility(View.INVISIBLE);
                Thread t = new Thread(() -> {
                    try {
                        while (true) {
                            if (mAudioModule == null || !mAudioModule.isPlaying()) {
                                if (lastActionResult == 1000) {
                                    runOnUiThread(() ->
                                            mOliveappStepHintText.setText(getString(R.string.hintverygood)));
                                    playAudio("oliveapp_step_hint_nextaction");
                                }
                                while (mAudioModule != null && mAudioModule.isPlaying()) {
                                    try {
                                        Thread.sleep(100L);
                                    } catch (InterruptedException var3) {
                                        var3.printStackTrace();
                                    }
                                }
                                runOnUiThread(() -> changeToNextAction(newActionType));
                                break;
                            }
                            try {
                                Thread.sleep(100L);
                            } catch (InterruptedException var2) {
                                var2.printStackTrace();
                            }
                        }
                    } catch (Exception var4) {
                    }
                });
                t.start();
            } else {
                changeToNextAction(newActionType);
            }
        } catch (Exception var8) {
        }

    }

    public void changeToNextAction(int newActionType) {
        try {
            String hintText;
            switch (newActionType) {
                case FacialActionType.UNKNOWN:
                    hintText = getString(R.string.oliveapp_step_hint_normal);
                    break;
                case FacialActionType.MOUTH_OPEN:
                    hintText = getString(R.string.oliveapp_step_hint_mouthopen);
                    break;
                case FacialActionType.EYE_CLOSE:
                    hintText = getString(R.string.oliveapp_step_hint_eyeclose);
                    break;
                case FacialActionType.HEAD_LEFT:
                    hintText = getString(R.string.oliveapp_step_hint_headleft);
                    break;
                case FacialActionType.HEAD_RIGHT:
                    hintText = getString(R.string.oliveapp_step_hint_headright);
                    break;
                case FacialActionType.HEAD_UP:
                    hintText = getString(R.string.oliveapp_step_hint_headup);
                    break;
                case FacialActionType.HEAD_DOWN:
                    hintText = getString(R.string.oliveapp_step_hint_headown);
                    break;
                case FacialActionType.HEAD_SHAKE_SIDE_TO_SIDE:
                    hintText = getString(R.string.oliveapp_step_hint_headshake);
                    break;
                default:
                    hintText = getString(R.string.oliveapp_step_hint_normal);
            }
            mOliveappStepHintImage.updateAnimation(
                    FacialActionType.getStepHintAnimationList(newActionType), 1000);
            playAudio(FacialActionType.getStringResourceName(newActionType));
            mOliveappStepHintText.setText(hintText);
            mVerificationController.nextAction();
            mOliveappCountDownProgressbar
                    .setRemainTimeSecond(10000, 10000);
            mOliveappCountDownProgressbar.setVisibility(View.VISIBLE);
        } catch (Exception var3) {
        }
    }

    @SuppressLint("SetTextI18n")
    public void onFrameDetected(int currentActionType, int actionState, int sessionState,
                                int remainingTimeoutMilliSecond) {
        mOliveappCountDownProgressbar
                .setRemainTimeSecond(remainingTimeoutMilliSecond, 10000);
    }

    protected void playAudio(String audioId) {
    }

    public void onLivenessSuccess(LivenessDetectionFrames livenessDetectionFrames) {
        try {
            playAudio("oliveapp_step_hint_verificationpass");
        } catch (Exception var3) {
        }
    }

    public void onLivenessFail(int result, LivenessDetectionFrames livenessDetectionFrames) {
        try {
            if (3 == result) {
                playAudio("oliveapp_step_hint_verificationfail");
            } else if (4 == result) {
                playAudio("oliveapp_step_hint_timeout");
            }
        } catch (Exception var4) {
        }
    }

    public void onPrestartFrameDetected(PrestartDetectionFrame frame, int remainingTimeMillisecond) {
    }

    public void onPrestartSuccess(LivenessDetectionFrames livenessDetectionFrames) {
        if (2 == mActionChangeMode) {
            Thread thread = new Thread(() -> {
                while (mAudioModule != null
                        && mAudioModule.isPlaying()) {
                    try {
                        Thread.sleep(100L);
                    } catch (InterruptedException var2) {
                        var2.printStackTrace();
                    }
                }
                mVerificationController.enterLivenessDetection();
            });
            thread.start();
        } else {
            mVerificationController.enterLivenessDetection();
        }
    }

    public void onPrestartFail(int result) {
    }
}
