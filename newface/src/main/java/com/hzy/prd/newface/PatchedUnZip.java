package com.hzy.prd.newface;

import com.hzy.prd.newface.unzip.UnzipPatch;

public final class PatchedUnZip {
    private static UnzipPatch mPatch;

    public static void setPatch(UnzipPatch patch) {
        mPatch = patch;
    }

    public static void unzip(String src, String dst) {
        if (mPatch != null) {
            try {
                mPatch.unzip(src, dst);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
