package com.hzy.newface.testapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.blankj.utilcode.constant.PermissionConstants;
import com.blankj.utilcode.util.PermissionUtils;
import com.blankj.utilcode.util.Utils;
import com.blankj.utilcode.util.ZipUtils;
import com.hzy.prd.newface.PatchedUnZip;

import androidx.annotation.Nullable;

public class SampleStartActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utils.init(getApplication());
        setContentView(R.layout.activity_sample_start);
        PatchedUnZip.setPatch(ZipUtils::unzipFile);

        findViewById(R.id.oliveappStartLivenessButton).setOnClickListener(v ->
                PermissionUtils.permission(PermissionConstants.CAMERA)
                        .callback(new PermissionUtils.SimpleCallback() {
                            @Override
                            public void onGranted() {
                                startActivity(new Intent(SampleStartActivity.this,
                                        SimpleDetectActivity.class));
                            }

                            @Override
                            public void onDenied() {
                            }
                        }).request());
    }
}
