package com.hzy.newface.testapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import com.hzy.prd.newface.BaseDetectionActivity;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.FacialActionType;
import com.oliveapp.face.livenessdetectorsdk.livenessdetector.datatype.LivenessDetectionFrames;

import java.io.File;
import java.io.FileOutputStream;


public class SimpleDetectActivity extends BaseDetectionActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setUsePredefinedConfig(2);
        setDetectionActions(FacialActionType.EYE_CLOSE, FacialActionType.HEAD_LEFT, FacialActionType.CAPTURE);
    }

    @Override
    public void onInitializeSucc() {
        mPhotoModule.setShutterRawDataCallback((bytes, cameraProxy) -> saveBitmapAndExit(bytes));
        startVerification();
    }

    private void saveBitmapAndExit(byte[] bytes) {
        new Thread() {
            @Override
            public void run() {
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                try {
                    FileOutputStream outputStream =
                            new FileOutputStream(new File(getExternalCacheDir().getAbsolutePath(), "out.jpg"));
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 85, outputStream);
                    outputStream.close();
                    bitmap.recycle();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    finish();
                }
            }
        }.start();
    }

    @Override
    public void onLivenessFail(int result, LivenessDetectionFrames livenessDetectionFrames) {
        super.onLivenessFail(result, livenessDetectionFrames);
        finish();
    }

    @Override
    public void onLivenessSuccess(LivenessDetectionFrames livenessDetectionFrames) {
        super.onLivenessSuccess(livenessDetectionFrames);
        mPhotoModule.captureWithCallBack();
    }
}
