# FaceLiveDetect
======================

我们维护一个活体识别SDK，针对我们的产品做定制修改

版本信息：
[![](https://jitpack.io/v/com.gitlab.fine3/FaceLiveDetect.svg)](https://jitpack.io/#com.gitlab.fine3/FaceLiveDetect)

### 接入方式
```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

```
dependencies {
        implementation 'com.gitlab.fine3:FaceLiveDetect:+'
}
```


2019年8月28日